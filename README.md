spark
=====

Super simple scripts to spark a server into life.

**Currently, only works on Debian-based OS as it uses apt.**

spark simply:
* copies the *spark* folder over to the server
* installs the packages listed in *spark/sapted* (using resapt)
* runs *spark/ran*

## How to use

### Clean Start

Simply run the command

```
./run_spark <hostname> [<user>] [...]
```
*`<hostname>` is the hostname or IP address of the server to SSH into and
`<user>` is the user SSH as (will sudo to install packages and run the
script. Any additional arguments will be passed to both scp and ssh when
connecting to the server.*

This will copy of the scripts, install restic, reclone and get things
ready to use *sapt* and *sran* (see below).

### Backup Start

Add a *.rclone.conf* file to the *spark* directory containing the config on
the *backup* remote and the file containing the password for the backup to
*spark/backup/id*, then run the *run_spark* command as above. It will do as
above, restore the backed up files, then *resapt* (see below) and the commands
stored in the */etc/ran* file.

### Custom Start

Put the packages you want installed into *spark/sapted* and the commands
that need to be run to set up the server into *spark/ran* and run the
*run_spark* command as above. It will do the same installing the packages
listed in *spark/sapted* and run the commands listed in *spark/ran*.

## Spark Scripts

### (re)sapt

The *sapt* and *resapt* scripts are simple wrappers around the apt command.
*sapt* will save any packages you install if successful to */etc/sapted*.
*resapt* will update the package lists, upgrade all packages, and install
all the packages in */etc/sapted*.

### sran

The *sran* script will run a bash shell with the HISTFILE set to */etc/ran* so
that all commands run will be stored to the */etc/ran* file. This file can
then be backed up using restic and will be run by spark when restoring a server
from backup.

## Example Files

Included in this repo and the *spark/sapted* and *spark/ran* files is an
example of getting (restic)[https://restic.readthedocs.io/] up and running
on the server. Restic is a fantasic way of backing up server configuration
files to make recovering the spark of another server even easier.
